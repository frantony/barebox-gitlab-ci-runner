#!/bin/sh

set -e

URL=$1
TOKEN=$2

DOCKER_IMAGE=frantony:barebox-debian11
DESCRIPTION="barebox-debian11"

if [ -z "$2" ]; then
	echo "Usage:"
	echo "register-runner.sh <URL> <TOKEN>"
fi


rm -rf gitlab-runner-config
mkdir -p gitlab-runner-config
touch gitlab-runner-config/config.toml

docker run --rm -t \
	-v $(pwd)/gitlab-runner-config:/etc/gitlab-runner \
	gitlab/gitlab-runner:latest \
		register \
		--non-interactive \
		--url "$URL" \
		--registration-token "$TOKEN" \
		--executor "docker" \
		--docker-image "$DOCKER_IMAGE" \
		--description "$DESCRIPTION" \
		--run-untagged="true" \
		--locked="false" \
		--access-level="not_protected" \
		--docker-pull-policy="never"
