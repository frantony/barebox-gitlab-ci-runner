#!/bin/sh

set -e

CONTAINER_NAME=gitlab-runner-barebox-debian11

docker run -d \
	--name "$CONTAINER_NAME" \
	--restart always \
	-v $(pwd)/gitlab-runner-config:/etc/gitlab-runner \
	-v /var/run/docker.sock:/var/run/docker.sock \
	gitlab/gitlab-runner:latest
