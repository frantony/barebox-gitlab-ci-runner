# Usage

Create docker build image:

```
IMAGE=frantony:barebox-debian11

docker build -t $IMAGE -f Dockerfile.debian11 .
```

Get registration token from ``GITLABURL/USERNAME/PROJECTNAME/-/settings/ci_cd`` -> Runners

Register runner with your ``GITLABURL`` and registration token, e.g.:

```
./register-runner.sh https://gitlab.com/ 2iTzy9RTAJdQyjcsxVYE
```

Start runner:

```
./start-runner.sh
```
