FROM debian:11

LABEL comment="barebox Debian 11 (Bullseye) build environment" \
      maintainer="Antony Pavlov <antonynpavlov@gmail.com>"

# Add default user
ENV BUILDUSER builduser
ENV WORKDIR /home/$BUILDUSER

RUN apt-get update \
	&& apt-get install -y \
		git less \
		make gcc libc6-dev \
		flex bison \
		inkscape imagemagick \
		bzip2 lzop lz4 pkg-config xz-utils \
		libusb-1.0-0-dev libssl-dev libncursesw5-dev \
		wget ca-certificates \
		gcc-mips-linux-gnu libc6-dev-mips-cross \
		gcc-arm-linux-gnueabi gcc-arm-linux-gnueabihf libc6-dev-armel-cross gcc-aarch64-linux-gnu \
		gcc-riscv64-linux-gnu libc6-dev-riscv64-cross bsdmainutils \
		python3-sphinx \
		sudo \
	&& apt-get install -y \
		python2 \
	&& apt-get clean \
	&& useradd -ms /bin/bash $BUILDUSER \
	&& adduser $BUILDUSER sudo \
	&& echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers.d/sudo-nopasswd

RUN wget --output-document - https://github.com/stffrdhrn/gcc/releases/download/or1k-10.0.0-20190723/or1k-linux-10.0.0-20190723.tar.xz | xzcat | tar xf - --strip 1 -C /usr/local \
	&& ln -s /usr/lib/x86_64-linux-gnu/libmpfr.so.6 /usr/lib/x86_64-linux-gnu/libmpfr.so.4

RUN apt-get install -y --no-install-recommends qemu-system-arm qemu-system-mips qemu-system-x86 qemu-system-common qemu-system-misc

#RUN apt-get install -y labgrid

RUN apt-get install -y python3-pip \
	&& cd /tmp \
	&& git clone --depth 1 https://github.com/labgrid-project/labgrid \
	&& cd labgrid \
	&& pip3 install -r requirements.txt \
	&& python3 setup.py install \
	&& cd .. && rm -rf labgrid \
	&& ln -s $(which pytest) /usr/local/bin/labgrid-pytest \
	&& pip3 install -U tuxmake \
    && apt-get install -y libyaml-libyaml-perl curl

RUN apt-get clean

USER $BUILDUSER

WORKDIR $WORKDIR
